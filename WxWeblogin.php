<?php
namespace duoge\wechat;

use yii\base\BaseObject;

class WxWeblogin extends BaseObject {

    public $appid;
    public $secret;

    public static function GET($appid,$secret,$code) {
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$appid}&secret={$secret}&code={$code}&grant_type=authorization_code";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ( $ch, CURLOPT_USERAGENT, "duogempwecat-sdk-php" );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $reponse = curl_exec($ch);
        if (curl_errno($ch))
        {
            throw new HttpException(0,curl_error($ch));
        }
        else
        {
            $httpStatusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if (200 !== $httpStatusCode)
            {
                print_r($httpStatusCode);
                //throw new HttpException($reponse,$httpStatusCode);
            }
        }
        curl_close($ch);
        return json_decode($reponse);
    }


}