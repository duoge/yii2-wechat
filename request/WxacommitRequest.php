<?php
namespace duoge\wechat\request;

class WxacommitRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "../wxa/commit";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setAccess_token($access_token) {
        $this->apiParas['access_token'] = $access_token;
    }

    public function setTemplate_id($template_id) {
        $this->apiParas['template_id'] = $template_id;
    }


    public function setext_json($ext_json) {
        $this->apiParas['ext_json'] = $ext_json;
    }

    public function setuser_version($user_version) {
        $this->apiParas['user_version'] = $user_version;
    }

    public function setuser_desc($user_desc) {
        $this->apiParas['user_desc'] = $user_desc;
    }
}