<?php
namespace duoge\wechat\request;
class ComponentToken extends WechatRequest {


    public function getApiMethodName()
    {
        return "component/api_component_token";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

    public function setComponent_appsecret($component_appsecret) {
        $this->apiParas['component_appsecret'] = $component_appsecret;
    }

    public function setComponent_verify_ticket($component_verify_ticket) {
        $this->apiParas['component_verify_ticket'] = $component_verify_ticket;
    }


}