<?php
namespace duoge\wechat\request;

class AddtemplateRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxaapi/newtmpl/addtemplate";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function settid($value) {
        $this->apiParas['tid'] = $value;
    }

    public function setkidList($value) {
        $this->apiParas['kidList'] = $value;
    }

    public function setsceneDesc($value) {
        $this->apiParas['sceneDesc'] = $value;
    }


}