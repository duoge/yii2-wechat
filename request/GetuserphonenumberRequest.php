<?php
namespace duoge\wechat\request;

class GetuserphonenumberRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "../wxa/business/getuserphonenumber";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setcode($code) {
        $this->apiParas['code'] = $code;
    }




}