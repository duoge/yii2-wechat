<?php
namespace duoge\wechat\request;

use yii\base\BaseObject;

class WechatRequest extends BaseObject {

    protected $apiParas = [];

    public function getApiMethodName()
    {
        return "";
    }

    public function get_method_type() {
        return 'GET';
    }


    public function putOtherTextParam($key, $value) {
        $this->apiParas[$key] = $value;
    }

    public function setBizContent($options) {
        $this->apiParas =  $options;
    }

    public function getApiParas()
    {
        return $this->apiParas;
    }




}