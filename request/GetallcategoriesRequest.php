<?php
namespace duoge\wechat\request;

class GetallcategoriesRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "wxopen/getallcategories";
    }

    public function get_method_type () {
        return 'GET';
    }
}