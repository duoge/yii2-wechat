<?php
namespace duoge\wechat\request;

class Minicode2sessionRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../sns/jscode2session";
    }


    public function setappid($appid) {
        $this->apiParas['appid'] = $appid;
    }

    public function setjs_code($js_code) {
        $this->apiParas['js_code'] = $js_code;
    }

    public function setgrant_type($grant_type) {
        $this->apiParas['grant_type'] = $grant_type;
    }
    public function setsecret($secret) {
        $this->apiParas['secret'] = $secret;
    }
}