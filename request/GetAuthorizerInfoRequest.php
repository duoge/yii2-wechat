<?php
namespace duoge\wechat\request;

class GetAuthorizerInfoRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "component/api_get_authorizer_info";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function iscomponent_access_token(){
        return true;
    }


    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

    public function setAuthorizer_appid($authorizer_appid) {
        $this->apiParas['authorizer_appid'] = $authorizer_appid;
    }
}