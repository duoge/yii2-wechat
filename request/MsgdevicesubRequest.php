<?php
namespace duoge\wechat\request;

class MsgdevicesubRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "message/device/subscribe/send";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function settemplate_id($template_id) {
        $this->apiParas['template_id'] = $template_id;
    }
    public function setto_openid_list($to_openid_list) {
        $this->apiParas['to_openid_list'] = $to_openid_list;
    }

    public function setsn($sn) {
        $this->apiParas['sn'] = $sn;
    }

    public function setdata($data) {
        $this->apiParas['data'] = $data;
    }

    public function setmodelId($modelId) {
        $this->apiParas['modelId'] = $modelId;
    }

    public function setpage($data) {
        $this->apiParas['page'] = $data;
    }

}