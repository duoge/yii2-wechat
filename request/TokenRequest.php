<?php
namespace duoge\wechat\request;



class TokenRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "token";
    }

    public function get_method_type() {
        return "GET";
    }

    public function set_grant_type($grant_type) {
        $this->apiParas["grant_type"] = $grant_type;
    }

    public function set_appid($appid) {
        $this->apiParas["appid"] = $appid;
    }

    public function set_secret($secret) {
        $this->apiParas["secret"] = $secret;
    }

}
