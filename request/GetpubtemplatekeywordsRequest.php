<?php
namespace duoge\wechat\request;

class GetpubtemplatekeywordsRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxaapi/newtmpl/getpubtemplatekeywords";
    }

    public function get_method_type () {
        return 'GET';
    }

    public function settid($value) {
        $this->apiParas['tid'] = $value;
    }


}