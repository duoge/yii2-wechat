<?php
namespace duoge\wechat\request;

class GrcodecreateRequest extends WechatRequest {

    public function get_method_type () {
        return 'POST';
    }

    public function getApiMethodName()
    {
        return "qrcode/create";
    }

    public function setExpire_seconds($expire_seconds) {
        $this->apiParas['expire_seconds'] = $expire_seconds;
    }

    public function setAction_name($action_name) {
        $this->apiParas['action_name'] = $action_name;
    }

    public function setScene_id($scene_id) {
        $this->apiParas['action_info'] = [
            "scene"=>[
                "scene_id"=>$scene_id
            ]
        ];
    }

    public function setScene_str($scene_str) {
        $this->apiParas['action_info'] = [
            "scene"=>[
                "scene_str"=>$scene_str
            ]
        ];
    }

}