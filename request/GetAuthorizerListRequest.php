<?php
namespace duoge\wechat\request;

class GetAuthorizerListRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "component/api_get_authorizer_list";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

    public function setOffset($offset) {
        $this->apiParas['offset'] = $offset;
    }

    public function setCount($count) {
        $this->apiParas['count'] = $count;
    }
}