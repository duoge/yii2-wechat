<?php
namespace duoge\wechat\request;

/**
 * 校验一张图片是否含有违法违规内容。详见内容安全解决方案

应用场景举例：

图片智能鉴黄：涉及拍照的工具类应用(如美拍，识图类应用)用户拍照上传检测；电商类商品上架图片检测；媒体类用户文章里的图片检测等；
敏感人脸识别：用户头像；媒体类用户文章里的图片检测；社交类用户上传的图片检测等。 频率限制：单个 appId 调用上限为 2000 次/分钟，200,000 次/天 （ 图片大小限制：1M ）
 *
 * https://developers.weixin.qq.com/miniprogram/dev/framework/security.imgSecCheck.html#HTTPS-%E8%B0%83%E7%94%A8
 */
class WxaImgSecCheckRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/img_sec_check";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setmedia($media) {
        $this->apiParas['media'] = $media;
    }
}