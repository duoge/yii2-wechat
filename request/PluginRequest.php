<?php
namespace duoge\wechat\request;

class PluginRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "../wxa/plugin";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setAccess_token($access_token) {
        $this->apiParas['access_token'] = $access_token;
    }

    public function setaction($action) {
        $this->apiParas['action'] = $action;
    }

    public function setplugin_appid($plugin_appid) {
        $this->apiParas['plugin_appid'] = $plugin_appid;
    }


}