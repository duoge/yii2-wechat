<?php
namespace duoge\wechat\request;

class WxasubmitauditRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/submit_audit";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setversion_desc($version_desc) {
        $this->apiParas['version_desc'] = $version_desc;
    }

    public function setfeedback_info($feedback_info) {
        $this->apiParas['feedback_info'] = $feedback_info;
    }
}