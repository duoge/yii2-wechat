<?php
namespace duoge\wechat\request;


/**
 * 模板消息
 * https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Template_Message_Interface.html
 */
class TmplmsgRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "message/template/send";
    }

    public function get_method_type() {
        return "POST";
    }

    public function setTouser($touser) {
        $this->apiParas['touser'] = $touser;
    }

    public function setTemplate_id($template_id) {
        $this->apiParas['template_id'] = $template_id;
    }

    public function setUrl($url) {
        $this->apiParas['url'] = $url;
    }

    public function setMiniprogram($miniprogram) {
        $this->apiParas['miniprogram'] = $miniprogram;
    }

    public function setAppid($appid) {
        $this->apiParas['appid'] = $appid;
    }

    public function setPagepath($pagepath) {
        $this->apiParas['pagepath'] = $pagepath;
    }

    public function setData($data) {
        $this->apiParas['data'] = $data;
    }

    public function setColor($color) {
        $this->apiParas['color'] = $color;
    }
}
