<?php
namespace duoge\wechat\request;

class DraftGetRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "draft/get";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setmedia_id($media_id) {
        $this->apiParas['media_id'] = $media_id;
    }


}