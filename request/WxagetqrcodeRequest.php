<?php
namespace duoge\wechat\request;

class WxagetqrcodeRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/get_qrcode";
    }

    public function get_method_type () {
        return 'GET';
    }

    public function setpath($path) {
        $this->apiParas['path'] = urlencode($path);
    }

}