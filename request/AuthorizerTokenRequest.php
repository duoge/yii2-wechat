<?php
namespace duoge\wechat\request;

class AuthorizerTokenRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "component/api_authorizer_token";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

    public function setAuthorizer_appid($authorizer_appid) {
        $this->apiParas['authorizer_appid'] = $authorizer_appid;
    }

    public function setAuthorizer_refresh_token($authorizer_refresh_token) {
        $this->apiParas['authorizer_refresh_token'] = $authorizer_refresh_token;
    }
}