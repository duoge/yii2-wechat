<?php
namespace duoge\wechat\request;

class DraftBatchgetRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "draft/batchget";
    }

    public function get_method_type () {
        return 'POST';
    }



    public function setoffset($offset) {
        $this->apiParas['offset'] = $offset;
    }

    public function setcount($count) {
        $this->apiParas['count'] = $count;
    }

    public function setno_content($no_content) {
        $this->apiParas['no_content'] = $no_content;
    }


}