<?php
namespace duoge\wechat\request;

class WxagetsnticketRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/getsnticket";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setsn($sn) {
        $this->apiParas['sn'] = $sn;
    }

    public function setmodel_id($model_id) {
        $this->apiParas['model_id'] = $model_id;
    }

}