<?php
namespace duoge\wechat\request;

class WxareleaseRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/release";
    }

    public function get_method_type () {
        return 'POST';
    }



}