<?php
namespace duoge\wechat\request;

class QueryauthRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "component/api_query_auth";
    }

    public function get_method_type () {
        return 'POST';
    }


    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

    public function setAuthorization_code($authorization_code) {
        $this->apiParas['authorization_code'] = $authorization_code;
    }


}