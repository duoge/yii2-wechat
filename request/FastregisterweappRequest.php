<?php
namespace duoge\wechat\request;

class FastregisterweappRequest extends WechatRequest {


    public function setname($name) {
        $this->apiParas['name'] = $name;
    }


    public function setcode($code) {
        $this->apiParas['code'] = $code;
    }

    public function setcode_type($code_type) {
        $this->apiParas['code_type'] = $code_type;
    }

    public function setlegal_persona_wechat($legal_persona_wechat) {
        $this->apiParas['legal_persona_wechat'] = $legal_persona_wechat;
    }

    public function setlegal_persona_name($legal_persona_name) {
        $this->apiParas['legal_persona_name'] = $legal_persona_name;
    }

    public function setcomponent_phone($component_phone) {
        $this->apiParas['component_phone'] = $component_phone;
    }

    public function getApiMethodName()
    {
        return "component/fastregisterweapp?action=create";
    }

    public function get_method_type () {
        return 'POST';
    }



}