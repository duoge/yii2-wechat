<?php
namespace duoge\wechat\request;

class PreAuthCodeRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "component/api_create_preauthcode";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setComponent_appid($component_appid) {
        $this->apiParas['component_appid'] = $component_appid;
    }

}