<?php
namespace duoge\wechat\request;

class SetprivacysettingRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "component/setprivacysetting";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setprivacy_ver($privacy_ver) {
        $this->apiParas['privacy_ver'] = $privacy_ver;
    }

    public function setsetting_list($setting_list) {
        $this->apiParas['setting_list'] = $setting_list;
    }

    public function setowner_setting($owner_setting) {
        $this->apiParas['owner_setting'] = $owner_setting;
    }

}