<?php
namespace duoge\wechat\request;

class UserinfoRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "user/info";
    }

    public function get_method_type () {
        return 'GET';
    }

    public function setOpenid($openid) {
        $this->apiParas['openid'] = $openid;
    }

    public function setAccess_token($access_token) {
        $this->apiParas['access_token'] = $access_token;
    }

}