<?php
namespace duoge\wechat\request;

/**
本接口用于异步校验图片/音频是否含有违法违规内容。

1.0 版本异步接口文档【点击查看】， 1.0 版本同步接口文档【点击查看】，1.0版本在2021年9月1日停止更新，请尽快更新至2.0

应用场景举例：

语音风险识别：社交类用户发表的语音内容检测；
图片智能鉴黄：涉及拍照的工具类应用(如美拍，识图类应用)用户拍照上传检测；电商类商品上架图片检测；媒体类用户文章里的图片检测等；
敏感人脸识别：用户头像；媒体类用户文章里的图片检测；社交类用户上传的图片检测等。 频率限制：单个 appId 调用上限为 2000 次/分钟，200,000 次/天；文件大小限制：单个文件大小不超过10M
 *
 * https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/sec-center/sec-check/mediaCheckAsync.html
 */
class WxaMediacheckRequest extends WechatRequest {

    public function getApiMethodName()
    {
        return "../wxa/media_check_async";
    }

    public function get_method_type () {
        return 'POST';
    }

    public function setmedia_url($media_url) {
        $this->apiParas['media_url'] = $media_url;
    }

    public function setmedia_type($media_type) {
        $this->apiParas['media_type'] = $media_type;
    }

    public function setversion($version) {
        $this->apiParas['version'] = $version;
    }

    public function setscene($scene) {
        $this->apiParas['scene'] = $scene;
    }

    public function setopenid($openid) {
        $this->apiParas['openid'] = $openid;
    }
}