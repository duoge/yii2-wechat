<?php
namespace duoge\wechat\request;

/**
 * 获取不限制的小程序码
 * https://developers.weixin.qq.com/miniprogram/dev/OpenApiDoc/qrcode-link/qr-code/getUnlimitedQRCode.html
 */
class WxagetwxacodeunlimitRequest extends WechatRequest {


    public function getApiMethodName()
    {
        return "../wxa/getwxacodeunlimit";
    }

    public function get_method_type() {
        return 'POST';
    }

    /**
     * 最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）
     * @param $scene
     * @return void
     */
    public function setscene($scene) {
        $this->apiParas['scene'] = $scene;
    }


    /**
     * 默认是主页，页面 page，例如 pages/index/index，根路径前不要填加 /，不能携带参数（参数请放在 scene 字段里），如果不填写这个字段，默认跳主页面。
     * @param $page
     * @return void
     */
    public function setpage($page) {
        $this->apiParas['page'] = $page;
    }

    /**
     * 默认430，二维码的宽度，单位 px，最小 280px，最大 1280px
     * @param $width
     * @return void
     */
    public function setwidth($width) {
        $this->apiParas['width'] = $width;
    }

    /**
     * 默认是{"r":0,"g":0,"b":0} 。auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"} 十进制表示
     * @param $line_color
     * @return void
     */
    public function setline_color($line_color) {
        $this->apiParas['line_color'] = $line_color;
    }

    //默认是false，是否需要透明底色，为 true 时，生成透明底色的小程序
    public function setis_hyaline($is_hyaline) {
        $this->apiParas['is_hyaline'] = $is_hyaline;
    }
}